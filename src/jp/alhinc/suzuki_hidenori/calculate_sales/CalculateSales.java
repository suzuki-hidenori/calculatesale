package jp.alhinc.suzuki_hidenori.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {
		if(args.length !=1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> branchNames = new HashMap<String, String>();
		HashMap<String, Long> sales = new HashMap<String, Long>();
		String regex = "^[0~9]{3}";
		String word = "支店";

		if(!inputFile(args[0], "branch.lst", branchNames, sales, regex, word)) {
			return;
		}
		File [] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++){
			String fileName = files[i].getName();
			if(fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		if(!processingFile(args[0], branchNames, sales, rcdFiles)) {
			return;
		}
		if(!outputFile(args[0], "branch.out", branchNames, sales )) {
			return;
		}
	}



	private static boolean inputFile(String filePath, String branchListFile, HashMap<String,String> branchMap, HashMap<String, Long> salesMap, String regex, String word) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, branchListFile);
			boolean branchFile = file.exists();
			if(!(branchFile)){
				System.out.println(word + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				String[] items = line.split(",");
				if(items.length != 2 || items[0].matches(regex)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				for(int i = 0; i < items.length; i++) {
					branchMap.put(items[0], items[1]);
					salesMap.put(items[0], 0L);
				}
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br != null ) {
				try {
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	private static boolean processingFile(String filePath, HashMap<String,String> branchMap, HashMap<String,Long> salesMap, List<File> filesRcd) {
		BufferedReader br = null;
		for(int i = 0; i < filesRcd.size() - 1; i++) {
			String names = (filesRcd.get(i)).getName();
			String numbers = names.substring(0,8);
			int fileNumbers = Integer.parseInt(numbers);

			String comparisonNames = (filesRcd.get(i + 1)).getName();
			String comparisonNumbers = comparisonNames.substring(0,8);
			int comparisonFileNumbers = Integer.parseInt(comparisonNumbers);

			if((comparisonFileNumbers) - fileNumbers   != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}
		for(int i = 0; i < filesRcd.size(); i++){
			try{
				br =  new BufferedReader(new FileReader(filesRcd.get(i)));
				ArrayList<String> branchSales = new ArrayList<>();
				String line;
				while((line = br.readLine()) != null){
					String salesFigures = line;
					// 例００１番の売り上げファイルの情報が読み込まれた後にlistに入れてその後、情報がいらなくなるので
					// リセットされて００２の売上ファイルの処理に移行する
					branchSales.add(salesFigures);
				}	// listに入れた支店番号
				if(branchSales.size() != 2){
					System.out.println(filesRcd.get(0)+ "のフォーマットが不正です");
					return false;
				}
				String branchCode = branchSales.get(0);
				if(!(branchMap.containsKey(branchCode))){
					System.out.println(filesRcd.get(i) + "の支店コードが不正です");
					return false;
				}
				// そもそも数字かどうかを判定する
				if(!(branchSales.get(1).matches("^([^[1-9][0-9]]*)$"))){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				// listに入れた売上金
				long fileSale = Long.parseLong(branchSales.get(1));

				// 合計金額を出すためにlistに入っている支店番号を出力して、それに売上金を上書きしていく。
				Long saleAmount = salesMap.get(branchCode) +  fileSale;
                      //Mapのデータに入れる。
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return false;
				}
				salesMap.put(branchCode, saleAmount);

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if(br != null ) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true;
	}


	private static boolean outputFile(String filePath, String branchOutFile, HashMap<String,String> branchMap, HashMap<String,Long> salesMap) {
		BufferedWriter bw = null;
		try{
			File file = new File(filePath, branchOutFile);
			bw = new BufferedWriter(new FileWriter(file));
			for(String key:branchMap.keySet()) {
				bw.write(key + "," +  branchMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null ) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
